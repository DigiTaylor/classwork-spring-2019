﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Barriers
{
    

    class MainClass
    {
        static int stride, height, width;

        public static void SetPixel(long x, long y, int r, int g, int b, byte[] pix)
        {
            long cur = stride * y + x;
            pix[cur++] = (byte)b;
            pix[cur++] = (byte)g;
            pix[cur] = (byte)r;

        }

        public static int[] GetPixel(long x, long y, byte[] pix)
        {
            int[] rgb = { (int)pix[y * stride + x + 2], (int)pix[y * stride + x + 1], (int)pix[y * stride + x] };
            return rgb;
        }

        public static void Blur(byte[] pix, byte[] pix2, int run)
        {
            for (long y = 0; y < height; ++y)
            {
                for (long x = 0; x < width; x += 3)
                {
                    int blurCount = 0;
                    int[] avg = { 0, 0, 0 };

                    for (long y2 = y - 6; y2 < y + 6; y2+=3)
                    {
                        for (long x2 = x - 6; x2 < x + 6; x2+=3)
                        {
                            long tmpx = (x2 >= 0 && x2 < width - 1) ? x2 : -x2;
                            long tmpy = (y2 >= 0 && y2 < height - 1) ? y2 : -y2;

                            if (tmpx < 0 || tmpx >= width || tmpy < 0 || tmpy >= height)
                                continue;
                            int[] tmpavg = (run % 2 == 0) ? GetPixel(tmpx, tmpy, pix) : GetPixel(tmpx, tmpy, pix2);


                            for (int i = 0; i < 3; ++i)
                            {
                                avg[i] += tmpavg[i];
                            }
                            blurCount++;
                        }
                    }

                    for (int i = 0; i < 3; ++i)
                    {
                        avg[i] /= blurCount;
                    }

                    if(run % 2 == 0)
                        SetPixel(x, y, avg[0], avg[1], avg[2], pix2);
                    else
                        SetPixel(x, y, avg[0], avg[1], avg[2], pix);

                }
            }
        }

        public static void Main(string[] args)
        {
            Bitmap img = (Bitmap)Image.FromFile(args[0]);

            var bdata = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            stride = bdata.Stride;
            Console.WriteLine(stride);
            height = bdata.Height;
            width = img.Width * 3;
            
            byte[] pix = new byte[stride * height];
            byte[] pix2 = new byte[stride * height];

            Marshal.Copy(bdata.Scan0, pix, 0, pix.Length);

            int runs = Int32.Parse(args[2]);

            for (int i = 0; i < runs; ++i)
            {
                Blur(pix, pix2, i);
            }

            if (runs % 2 == 0)
                Marshal.Copy(pix, 0, bdata.Scan0, pix.Length);
            else
                Marshal.Copy(pix2, 0, bdata.Scan0, pix2.Length);

            img.UnlockBits(bdata);

            img.Save("out.png");
        }
    }
}
