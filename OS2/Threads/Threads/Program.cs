﻿using System;
using System.IO;
using System.Threading;
using System.Net;
using System.Linq;
using System.Text.RegularExpressions;

class Threads
{
    public static void Main(string[] args)
    {
        if (args == null || args.Length != 1)
        {
            throw new ApplicationException("Specify file path, be sure to only list one file path.");
        }
        string[] urls = File.ReadAllLines(args[0]);
        foreach (var line in urls)
        {
            var T = new Thread(() =>
            {
                //Read each line in urls and grab last segment to get filename if any
                Uri site = new Uri(line);
                string newFilename = site.Segments.LastOrDefault();

                //Trim down URI to get the plain host name without www. or 
                //the trailing file path
                string hostNameTrimmed = site.Host.Replace("www.", "");
                hostNameTrimmed = Regex.Replace(hostNameTrimmed, @"(\..+)+", "");

                //If named file doesn't exist, rename it to index.html and 
                //prepend forward slash either way
                if (newFilename == "/")
                {
                    newFilename = @"/index.html";
                }
                else
                {
                    newFilename = @"/" + newFilename;
                }

                //Create a directory named saved_sites and multiple directories
                //based on each individual sites.
                string newFileDir = @"./saved_sites/" + hostNameTrimmed;

                if (!Directory.Exists(newFileDir))
                {
                    Directory.CreateDirectory(newFileDir);
                    Console.WriteLine(newFileDir);
                }

                //Save the webpage to the previouly created directories
                string newFilePath = newFileDir + newFilename;
                WebClient client = new WebClient();
                client.DownloadFile(line, newFilePath);
            });

            T.Start();
            T.Join();
        }
    }
}