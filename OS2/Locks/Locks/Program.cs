﻿using System;
using System.IO;
using System.Threading;
using System.Net;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

class Threads
{

    static Object L = new object();

    static List<Thread> threadList = new List<Thread>();

    static int[] status;

    static string[] threadIDList;

    static int threadID = 0;

    public static void startThreads()
    {
        foreach(var t in threadList)
        {
            t.Start();
        }
    }

    public static void printStatus()
    {
        bool inProg = true;
        while(inProg)
        {
            lock (L)
            {
                for (int index = 0; index < status.Length && index < threadIDList.Length; index++)
                {
                    //Prints status of downloads as long as threadIDList is filled
                    if (!threadIDList.Contains(null))
                    {
                        Console.Write(threadIDList[index] + ": ");

                        switch (status[index])
                        {
                            case -1:
                                Console.WriteLine("Error!");
                                break;

                            case 0:
                                Console.WriteLine("Unintialized!");
                                break;

                            case 1:
                                Console.WriteLine("In Progress!");
                                break;

                            case 2:
                                Console.WriteLine("Complete!");
                                break;

                            default:
                                break;
                        }
                    }
                    //Breaks if all downloads are finished
                    inProg = (status.Contains(0) || status.Contains(1));
                }

                for (int i = 0; i < 80; ++i)
                {
                    Console.Write("=");
                }

                Console.WriteLine();

            }
            Thread.Sleep(500);
        }

    }

    public static void Main(string[] args)
    {

        if (args == null || args.Length != 1)
        {
            throw new ApplicationException("Specify file path, be sure to only list one file path.");
        }

        string[] urls = File.ReadAllLines(args[0]);

        status = new int[urls.Length];

        //Initialize status array to uninitialized
        for (int i = 0; i < status.Length; ++i)
        {
            status[i] = 0;
        }

        threadIDList = new string[urls.Length];

        foreach (var line in urls)
        {
            threadList.Add(new Thread(() =>
            {
                int local_id;

                lock (L)
                {
                    //Set status of thread to In Progress and give it a unique 
                    //URL ID and ID number.
                    local_id = threadID;
                    threadID++;
                    threadIDList[local_id] = line;
                    status[local_id] = 1;
                }

                //Read each line in urls and grab last segment to get filename if any
                Uri site = new Uri(line);
                string newFilename = site.Segments.LastOrDefault();

                //Trim down URI to get the plain host name without www. or 
                //the trailing file path
                string hostNameTrimmed = site.Host.Replace("www.", "");
                string hostNameTrimmed2 = Regex.Replace(hostNameTrimmed, @"(\..+)+", "");

                //If named file doesn't exist, rename it to index.html and 
                //prepend forward slash either way
                if (newFilename == "/")
                {
                    newFilename = @"/index.html";
                }
                else
                {
                    newFilename = @"/" + newFilename;
                }

                //Create a directory named saved_sites and multiple directories
                //based on each individual sites.
                string newFileDir = @"./saved_sites/" + hostNameTrimmed2;

                if (!Directory.Exists(newFileDir))
                {
                    Directory.CreateDirectory(newFileDir);
                }

                //Save the webpage to the previouly created directories
                string newFilePath = newFileDir + newFilename;

                WebClient client = new WebClient();
                try
                {
                    client.DownloadFile(line, newFilePath);
                    lock(L)
                    {
                        status[local_id] = 2;
                    }
                }
                catch (Exception e)
                {
                    lock(L)
                    {
                        status[local_id] = -1;
                    }
                }
            }));
        }

        startThreads();

        printStatus();

        lock(L)
        {
            for (int i = 0; i < status.Length; ++i)
            {
                if(status[i] == -1)
                {
                    Console.WriteLine("Error downloading: {0}", threadIDList[i]);
                }
            }
        }

        Console.WriteLine("DOWNLOADS FINISHED...");
    }
}