//Name: Trent Shubirg && Alec Taylor
//Class: ETEC 3201
//Assignment: Lab 1 - Receiver

int light = HIGH;

void setup() {
    Serial.begin(9600);
    pinMode(7, INPUT);
    pinMode(13, OUTPUT);
}

void loop() {
    if(digitalRead(7) == HIGH)
    {
      digitalWrite(13, HIGH);
      Serial.println("HIGH");
    }
    else if(digitalRead(7) == LOW)
    {
      digitalWrite(13, LOW);
      Serial.println("LOW");
    }
}
