void setup() {
  pinMode(13, OUTPUT);
  pinMode(7, INPUT);
  Serial.begin(9600);
  digitalWrite(13, HIGH);
}

void loop() {
  char c = 0;
  bool brk = false;

  if(digitalRead(7) == LOW)
  {
    delayMicroseconds(700);

    for(int i = 7; i >= -1; i--)
    {
      if(i != -1)
        c |= (digitalRead(7) << i);
      else
        if(digitalRead(7) == LOW)
        {
          Serial.println("FRAME ERROR");
          brk = true;
        }
      delayMicroseconds(480);
    }
    if(c == 0 && brk)
      Serial.println("BREAK");
    else
      Serial.print(c);
  }

}
