//Alec Taylor, Hunter Bebout
//Receiver
//ETEC 3201
//2-6-2019

void setup() {
  Serial.begin(9600);
  pinMode(11, INPUT);
  pinMode(12, INPUT);
  pinMode(13, OUTPUT);
}
char c = 0;
int bitNum = 7;
void loop() {
  while(digitalRead(11) == HIGH)
  {}
  digitalWrite(13, digitalRead(12));
  c |= (digitalRead(12) << bitNum);
  if(bitNum == 0)
  {
    bitNum = 7;
    Serial.print(c);
    c = 0;
  }
  else
  {
    bitNum--;
  }
  
  while(digitalRead(11) == LOW)
  {}
  
}
